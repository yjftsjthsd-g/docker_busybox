FROM debian as build
LABEL maintainer "Brian Cole <docker@brianecole.com>"

ARG BUSYBOX_VERSION=1.31.1

RUN apt-get update && apt-get install -yq build-essential wget
RUN mkdir /opt/busybox
WORKDIR /opt/busybox
RUN wget --no-verbose -O busybox.tar.bz2 https://www.busybox.net/downloads/busybox-$BUSYBOX_VERSION.tar.bz2
RUN tar -xvj --strip-components 1 -f busybox.tar.bz2
RUN make defconfig && sed -i 's/# CONFIG_STATIC is not set/CONFIG_STATIC=y/' .config
RUN make
RUN mkdir -p /opt/busybox/bin && cp /opt/busybox/busybox /opt/busybox/bin/


FROM scratch AS assemble
LABEL maintainer "Brian Cole <docker@brianecole.com>"

COPY --from=build /opt/busybox/bin /bin
WORKDIR /
RUN ["/bin/busybox", "--install", "-s", "/bin"]


FROM scratch AS final
LABEL maintainer "Brian Cole <docker@brianecole.com>"

COPY --from=assemble / /
WORKDIR /

ENV PATH /bin

ENTRYPOINT ["/bin/sh"]
