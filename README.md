# docker_busybox

In which I create a busybox root image.

You probably shouldn't use this; there's an official busybox image that's
probably better maintained, possibly better crafted, and pretty likely does the
exact same thing. I'm doing this so I know how to do it so I can go make images
of other distros, but it's probably not useful unto itself.
